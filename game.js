'use strict';

class Vector {
    constructor(x=0, y=0) {
        this.x = x;
        this.y = y;
    }

    // создает новый вектор
    // координаты нового вектора равны сумме координат суммируемых
    plus(vector) {
        if (!(vector instanceof Vector)) {
            throw new Error('Можно прибавлять к вектору только вектор типа Vector');
        }

        return new Vector(vector.x + this.x, vector.y + this.y);
    }

    // создает новый вектор
    // координаты нового вектора увеличины в n раз
    times(multiplier) {
        return new Vector(this.x * multiplier, this.y * multiplier);
    }

}

class Actor {
    constructor (pos = new Vector(0, 0), size = new Vector(1, 1), speed = new Vector(0, 0)) {
        if (!(pos instanceof Vector)) {
            throw new Error('Аргумент pos не является экземпляром класса Vector');
        }

        if (!(size instanceof Vector)) {
            throw new Error('Аргумент size не является экземпляром класса Vector');
        }

        if (!(speed instanceof Vector)) {
            throw new Error('Аргумент speed не является экземпляром класса Vector');
        }

        this.pos = pos;
        this.size = size;
        this.speed = speed;
    }

    act() {
    
    }

    get left() {
        return this.pos.x;
    }
    
    get top() {
        return this.pos.y;
    }
    
    get right() {
        return this.pos.x + this.size.x;
    }
    
    get bottom() {
        return this.pos.y + this.size.y;
    }

    get type() {
        return 'actor';
    }

    isIntersect(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error('Можно передавать только экземпляры типа Actor');
        }

        if (actor === this) {
            return false;
        }
        
        if (this.left >= actor.right) {
            return false;
        }

        if (this.right <= actor.left) {
            return false;
        }

        if (this.bottom <= actor.top) {
            return false;
        }

        if (this.top >= actor.bottom) {
            return false;
        }

        return true;
    }
}

class Level {
    constructor(grid = [], actors = []) {
        this.grid = grid;
        this.actors = actors;
        this.player = this.actors.find(actor => actor.type === 'player');
        this.height = grid.length;
        this.width = Math.max(0, ...grid.map(item => item.length));
        this.status = null;
        this.finishDelay = 1;
    }

    isFinished() {
        if ((this.status !== null) && this.finishDelay < 0) {
            return true;
        }
        return false;
    }

    actorAt(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error('Аргумент actor не является экземпляром класса Actor');
        }

        return this.actors.find(actorItem => actor.isIntersect(actorItem));
    }

    obstacleAt(position, size) {
        if (!(position instanceof Vector)) {
            throw new Error('Аргумент position не является экземпляром класса Vector');
        }

        if (!(size instanceof Vector)) {
            throw new Error('Аргумент size не является экземпляром класса Vector');
        }

        let left = Math.floor(position.x);
        let right = Math.ceil(position.x + size.x);
        let top = Math.floor(position.y);
        let bottom = Math.ceil(position.y + size.y);

        if (left < 0 || right > this.width || top < 0) {
            return 'wall';
        }

        if (bottom > this.height) {
            return 'lava';
        }

        for (let horizontal = left; horizontal < right; horizontal++) {
            for (let vertical = top; vertical < bottom; vertical++) {
                let cell = this.grid[vertical][horizontal];
                if (cell) {
                    return cell;
                }
            }
        }
    }

    removeActor(actor) {
        this.actors.splice(this.actors.indexOf(actor), 1);
    }

    noMoreActors(actorType) {
        if (this.actors.filter(actor => actor.type === actorType).length <= 0) {
            return true;
        } else {
            return false;
        }
    }

    playerTouched(obstacleType = '', actor) {
        if (this.status !== null) {
            return false;
        }

        if (obstacleType === 'lava' || obstacleType === 'fireball') {
            this.status = 'lost';
            return false;
        }
        
        if ((obstacleType === 'coin')) {
            this.removeActor(actor);
            if (this.noMoreActors(obstacleType)) {
                this.status = 'won';
            }
        }  
    }
}

class LevelParser {
    constructor(actorObject = {}) {
        this.actorsMap = Object.create(actorObject);
        this.obstaclesMap = {'x': 'wall', '!': 'lava'};
    }

    actorFromSymbol(symbol) {
        return this.actorsMap[symbol];
    }

    obstacleFromSymbol(symbol) {
        return this.obstaclesMap[symbol];
    }

    createGrid(plan = []) {
        return plan.map(row => row.split('').map(item => this.obstacleFromSymbol(item)));
    }

    createActors(plan = []) {
        let actors = [];
        plan.forEach((strOfPlan, firstIndex) => {
            strOfPlan.split('').forEach((symbol, index) => {
                let constructorOfActor = this.actorFromSymbol(symbol);
                if (typeof constructorOfActor === 'function') {
                    let actor = new constructorOfActor(new Vector(index, firstIndex));
                    if (actor instanceof Actor) {
                        actors.push(actor);
                    }
                }
            });
        });
        return actors;
    }

    parse(plan) {
        return new Level(this.createGrid(plan), this.createActors(plan));
    }
}

class Fireball extends Actor {
    constructor(pos = new Vector(0, 0), speed = new Vector(0, 0)) {
        super(pos, new Vector(1, 1), speed);
    }

    get type() {
        return 'fireball';
    }

    getNextPosition(time=1) {
        return this.pos.plus(this.speed.times(time));
    }

    handleObstacle() {
        this.speed = this.speed.times(-1);
    }
    
    act(time, level) {
        let pos = this.getNextPosition(time);
        if (level.obstacleAt(pos, this.size)) {
            this.handleObstacle();
        } else {
            this.pos = pos;
        }
    }
}

class HorizontalFireball extends Fireball {
    constructor(pos = new Vector(0, 0)) {
        super(pos, new Vector(2, 0));
    }
}

class VerticalFireball extends Fireball {
    constructor(pos = new Vector(0, 0)) {
        super(pos, new Vector(0, 2));
    }
}

class FireRain extends Fireball {
    constructor(pos = new Vector(0, 0)) {
        super(pos, new Vector(0, 3));
        this.oldPosition = this.pos;
    }

    handleObstacle() {
        this.pos = this.oldPosition;
    }
}

class Coin extends Actor {
    constructor(position = new Vector(0, 0)) {
        let startPosition = position.plus(new Vector(0.2, 0.1));
        super(startPosition, new Vector(0.6, 0.6));
        this.position = startPosition;
        this.spring = Math.random() * 2 * Math.PI;
        this.springSpeed = 8;
        this.springDist = 0.07;
    }

    get type() {
        return 'coin';
    }
    
    updateSpring(time = 1) {
        this.spring += this.springSpeed * time;
    }
    
    getSpringVector() {
        return new Vector(0, Math.sin(this.spring) * this.springDist);
    }
    
    getNextPosition(time = 1) {
        this.updateSpring(time);
        return this.position.plus(this.getSpringVector());
    }
    
    act(time) {
        this.pos = this.getNextPosition(time)
    }
}

class Player extends Actor {
    constructor(pos = new Vector(0, 0)) {
        super(pos.plus(new Vector(0, -0.5)), new Vector(0.8, 1.5));
    }
    
    get type() {
        return 'player';
    }
}

const schemas = [
    [
      '         ',
      '         ',
      '    =    ',
      '       o ',
      '     !xxx',
      ' @       ',
      'xxx!     ',
      '         '
    ],
    [
      '      v  ',
      '    v    ',
      '  v      ',
      '        o',
      '        x',
      '@   x    ',
      'x        ',
      '         '
    ]
];

const actorDict = {
    '@': Player,
    'o': Coin,
    'v': FireRain,
    '|': VerticalFireball,
    '=': HorizontalFireball
};
  
const parser = new LevelParser(actorDict);
runGame(schemas, parser, DOMDisplay)
.then(() => console.log('Вы выиграли приз!'));